<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Message;

class MessagesController extends Controller
{
    public function submit(Request $request){
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required'
        ]);

        // Create New Message
        $msg = new Message;
        $msg->name = $request->input('name');
        $msg->email = $request->input('email');
        $msg->message = $request->input('msg');

        //Save Message
        $msg->save();

        //Redirect
        return redirect('/')->with('success','Message send successfully');
    }

    public function getMessages(){
        $messages = Message::all();

        return view('viewmessages')->with('messages',$messages);
    }
}
