@extends('layouts.main')

@section('content')
    <h1>Messages</h1>
    @if(count($messages)>0)
        @foreach($messages as $msg)
            <div class="well bg-success">
                <label>Name</label> : {{$msg->name}}<br>
                <label>Email</label> : {{$msg->email}}<br>
                <label>Message</label> : {{$msg->message}}
            </div>
        @endforeach
    @endif
@endsection